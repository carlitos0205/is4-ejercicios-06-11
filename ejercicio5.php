<!-- CREATE TABLE tabla_ejercicio4 (
    id serial PRIMARY KEY,
    nombre character varying(40),
    descripcion character varying(250)
); -->

<?php
// Datos de conexión a la base de datos
$host = "localhost"; // O el nombre del servidor donde se encuentra la
base de datos
$port = "5432"; // Ajusta el puerto si es diferente
$dbname = "ejercicio4";
$user = "tu_usuario"; // Reemplaza con tu usuario de PostgreSQL
$password = "tu_contraseña"; // Reemplaza con tu contraseña

try {
    // Realiza la conexión a la base de datos utilizando PDO
    $pdo = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");

    // Inserta 1000 registros aleatorios en la tabla
    $insertStatement = $pdo->prepare("INSERT INTO tabla_ejercicio4
(nombre, descripcion) VALUES (?, ?)");
    $insertStatement->bindParam(1, $nombre);
    $insertStatement->bindParam(2, $descripcion);

    for ($i = 0; $i < 1000; $i++) {
        $nombre = bin2hex(random_bytes(20)); // Genera una cadena
aleatoria de 40 caracteres
        $descripcion = bin2hex(random_bytes(125)); // Genera una
cadena aleatoria de 250 caracteres
        $insertStatement->execute();
    }

    // Realiza una consulta para obtener los registros insertados
    $query = "SELECT * FROM tabla_ejercicio4";
    $result = $pdo->query($query);

    // Muestra los resultados en una tabla HTML
    echo "<table>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripción</th>
            </tr>";

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['nombre'] . "</td>";
        echo "<td>" . $row['descripcion'] . "</td>";
        echo "</tr>";
    }

    echo "</table>";
} catch (PDOException $e) {
    die("Error al conectar a la base de datos: " . $e->getMessage());
}
?>
