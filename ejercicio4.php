<?php
// Datos de conexión a la base de datos
$host = "localhost"; // O el nombre del servidor donde se encuentra la
base de datos
$port = "5435";
$dbname = "ejercicio1";
$user = "sistema";
$password = "sistema"; // Cambia esto si tu contraseña es diferente

try {
    // Realiza la conexión a la base de datos utilizando PDO
    $pdo = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");

    // Prepara la consulta SQL
    $query = "SELECT p.nombre_producto, p.precio_producto,
m.nombre_marca, e.nombre_empresa, c.nombre_categoria
              FROM productos AS p
              JOIN marcas AS m ON p.id_marca = m.id
              JOIN empresas AS e ON p.id_empresa = e.id
              JOIN categorias AS c ON p.id_categoria = c.id";

    $statement = $pdo->prepare($query);

    // Ejecuta la consulta
    $statement->execute();

    // Muestra los resultados en una tabla HTML
    echo "<table>
            <tr>
                <th>Nombre del Producto</th>
                <th>Precio del Producto</th>
                <th>Nombre de la Marca</th>
                <th>Nombre de la Empresa</th>
                <th>Nombre de la Categoría</th>
            </tr>";

    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>" . $row['nombre_producto'] . "</td>";
        echo "<td>" . $row['precio_producto'] . "</td>";
        echo "<td>" . $row['nombre_marca'] . "</td>";
        echo "<td>" . $row['nombre_empresa'] . "</td>";
        echo "<td>" . $row['nombre_categoria'] . "</td>";
        echo "</tr>";
    }

    echo "</table>";
} catch (PDOException $e) {
    die("Error al conectar a la base de datos: " . $e->getMessage());
}
?>
